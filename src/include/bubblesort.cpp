#include <iostream>
#include <sstream>
#include <string>

#include "bubblesort.hpp"

/*
    Конструктор по умолчанию
*/
BubbleSort::BubbleSort()
{

}


/*
    Конструктор класса BubbleSort, принимающий массив (вектор) целых чисел
*/
BubbleSort::BubbleSort(std::vector<int> array) : array(array)
{
}

void BubbleSort::sort() 
{
    if (array.empty()) return;

    for(auto it = array.begin(); it != (array.end() - 1); ++it)
    {
        for(auto jt = array.end() - 1; jt != (it); --jt)
        {
            if (*jt < *(jt - 1))
            {
                int current = *jt;
                *jt = *(jt - 1);
                *(jt - 1) = current;
            }
        }
    }
}

void BubbleSort::print() 
{
    for(auto it = array.begin(); it != array.end(); ++it)
    {
        std::cout << *it << " ";
    }

    std::cout << std::endl;
}

void BubbleSort::read()
{    
    std::string line = "";
    std::getline(std::cin, line);
    std::istringstream ssin(line);
        
    int input;

    while(ssin >> input)
    {
        array.push_back(input);
    }

}

std::vector<int> BubbleSort::getArray()
{
    return array;
}