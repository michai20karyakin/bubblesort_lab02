#!/bin/bash
set -e

VERSION=$(cat version)
STAGING_DIR="cicd/bubblesort_${VERSION}_amd64"
DEB_FILE="$STAGING_DIR.deb"


mkdir $STAGING_DIR
cp -r usr $STAGING_DIR/usr
cp -r DEBIAN $STAGING_DIR/DEBIAN
dpkg-deb --build --root-owner-group $STAGING_DIR || rm $DEB_FILE

if test -f $DEB_FILE; then
    mv $DEB_FILE "."
fi

if test -d $STAGING_DIR; then
    rm -rf $STAGING_DIR
fi
