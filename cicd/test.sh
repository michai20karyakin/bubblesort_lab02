#!/bin/bash  
set -e

ECHO_INPUT="Входной массив:"
ECHO_OUTPUT="Вывод программы:"

function Test {
    echo $ECHO_INPUT $arr
    OUT="$(echo $arr | usr/bin/bubblesort | tail -n1 | head -c -2)"

    echo $ECHO_OUTPUT $OUT
    (echo $arr | (usr/bin/bubblesort > /dev/null 2> /dev/null)) 2> /dev/null
    PROGRAM_RESULT=$?
    if [ $PROGRAM_RESULT != 0 ]
    then
        echo "TEST FAIL"
        exit 1 
    fi

    if [ "$ok_arr" == "$OUT" ]
    then
        echo "TEST OK"
    else
        echo "TEST FAIL"
        exit 2 
    fi

}


arr="5 0 -4 -5 21 43234 43"
#arr=""
ok_arr="-5 -4 0 5 21 43 43234"
echo "Test non empty array"
Test

arr=""
ok_arr=""
echo "Test empty array"
Test